# Introduction
All code required to control the PBv3 testing setup.

# Quick Start
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/berkeleylab/labremote-apps/powertools.git
cd powertools
mkdir build
cd build
cmake3 ..
make
```

# Repository Structure
`labremote/` - submodule with LabRemote library
`pbv3/` - source code for the `PBv3` library
`pbv3/tools` - programs for testing a single powerboard
`pbv3/active` - programs for running tests on the massive tester
