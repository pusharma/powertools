# - Try to find ITSDAQ
# Once done this will define
#  ITSDAQ_FOUND - System has libftdi1
#  ITSDAQ_INCLUDE_DIRS - The libftdi1 include directories
#  ITSDAQ_LIBRARIES - The libraries needed to use libftdi1
#  ITSDAQ_DEFINITIONS - Compiler switches required for using libftdi1

FIND_PATH(ITSDAQ_INCLUDE_DIR hsio/hsio_c.h
  HINTS /usr/include /usr/local/include ${ITSDAQ_DIR} )

FIND_LIBRARY(ITSDAQ_HSIO_LIBRARY NAMES hsio_lib libhsio_lib
  HINTS /usr/lib64 /usr/local/lib /usr/lib/x86_64-linux-gnu ${ITSDAQ_DIR}
  PATH_SUFFIXES build)

FIND_LIBRARY(ITSDAQ_STDLL_LIBRARY NAMES stdll libstdll
  HINTS /usr/lib64 /usr/local/lib /usr/lib/x86_64-linux-gnu ${ITSDAQ_DIR}
  PATH_SUFFIXES build)

INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set ITSDAQ_FOUND to TRUE
# if all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ITSDAQ DEFAULT_MSG
  ITSDAQ_HSIO_LIBRARY ITSDAQ_STDLL_LIBRARY ITSDAQ_INCLUDE_DIR)

MARK_AS_ADVANCED(ITSDAQ_INCLUDE_DIR ITSDAQ_LIBRARY )

SET(ITSDAQ_LIBRARIES ${ITSDAQ_HSIO_LIBRARY} ${ITSDAQ_STDLL_LIBRARY} )
SET(ITSDAQ_INCLUDE_DIRS ${ITSDAQ_INCLUDE_DIR} )
