#ifndef ENDEAVOURRAWHSIO_H
#define ENDEAVOURRAWHSIO_H

#include <memory>

#include "hsio/hsio_c.h"
#include "hsio/hsio_full.h"

#include "EndeavourRaw.h"

#define HSIO_ECHO_OPCODE 0x03
#define HSIO_MORSE_OPCODE 0x8E

class EndeavourRawHSIO : public EndeavourRaw
{
public:
  EndeavourRawHSIO();
  ~EndeavourRawHSIO();

  //
  // Endeavour communication
  void setDitMin(uint32_t DIT_MIN);
  uint32_t getDitMin();

  void setDitMid(uint32_t DIT_MID);
  uint32_t getDitMid();

  void setDitMax(uint32_t DIT_MAX);  
  uint32_t getDitMax();

  void setDahMin(uint32_t DAH_MIN);
  uint32_t getDahMin();

  void setDahMid(uint32_t DAH_MID);
  uint32_t getDahMid();

  void setDahMax(uint32_t DAH_MAX);  
  uint32_t getDahMax();

  void setBitGapMin(uint32_t BITGAP_MIN);
  uint32_t getBitGapMin();

  void setBitGapMid(uint32_t BITGAP_MID);
  uint32_t getBitGapMid();

  void setBitGapMax(uint32_t BITGAP_MAX);  
  uint32_t getBitGapMax();
  
  void reset();

  void sendData(unsigned long long int data, unsigned int size);

  bool isError();
  bool isDataValid();
  void readData(unsigned long long int& data, unsigned int& size);

private:
  // Last returned data
  bool m_valid=false;
  bool m_error=false;

  unsigned long long int m_readData;
  unsigned int m_readSize;

  // HSIO communication
  std::unique_ptr<HsioC> m_access;
  uint16_t m_seqnum=0;
};

#endif //ENDEAVOURRAWHSIO_H
