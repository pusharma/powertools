#ifndef ITSDAQPACKET_H_
#define ITSDAQPACKET_H_

#include <vector>
#include <cstdint>

#include "ITSDAQOpCode.h"

class ITSDAQPacket
{
public:
  ITSDAQPacket(const std::vector<uint16_t>& rawdata);
  ITSDAQPacket(uint16_t seqNum, const std::vector<ITSDAQOpCode>& opcodes);


  uint16_t checksum() const;

  std::vector<uint16_t> rawdata() const;

  uint16_t nOpcodes() const;
  ITSDAQOpCode opcode(uint16_t idx) const;
  
  
private:
  std::vector<uint16_t> m_packet;
  std::vector<ITSDAQOpCode> m_opcodes;
};

#endif // ITSDAQPACKET_H_
