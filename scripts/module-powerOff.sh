#!/bin/bash

#
# Disable the power on a X hybrid
#

#
# Disable the Hybrid

# resetB
./bin/endeavour write 46 0x000000
./bin/endeavour write 47 0x000000

# Hybrid LDOs
./bin/endeavour write 41 0x66660700
./bin/endeavour write 40 0x66660700

#
# Disable the DCDC
./bin/endeavour write 42 0
./bin/endeavour write 43 0
