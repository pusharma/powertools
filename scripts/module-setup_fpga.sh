#!/bin/bash

#
# Simple script to configure the NEXYS Video FPGA
# Written by Matthew Gignac (SCIPP)
#

## Configure FGPA
echo " >>>>>>>>> LISTING FGPA DEVICES <<<<<<<<< " 
djtgcfg enum

echo " >>>>>>>>> INITIALIZING THE NEXYSVIDEO DEVICE <<<<<<<<< "
djtgcfg init -d NexysVideo

echo " >>>>>>>>> PROGRAMMING THE NEXYSVIDEO DEVICE <<<<<<<<< "

FWNAME=nexysv_itsdaq_vb444_FDP_STAR.bit

if [ ! -e ${FWNAME} ]; then
    wget http://www.hep.ucl.ac.uk/~warren/upgrade/firmware/${FWNAME}
fi
djtgcfg prog -d NexysVideo -i 0 --file ${FWNAME}


