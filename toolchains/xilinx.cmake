set(XILINX_SDK_PATH "/opt/Xilinx/SDK/2019.1" CACHE STRING "Path to Xilinx SDK with compilers.")
set(ROOTFS_PATH "" CACHE STRING "Path to Petalinux system root.")

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(CMAKE_SYSROOT ${ROOTFS_PATH})

SET(tools ${XILINX_SDK_PATH}/gnu/aarch32/lin/gcc-arm-linux-gnueabi)
set(CMAKE_C_COMPILER ${tools}/bin/arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER ${tools}/bin/arm-linux-gnueabihf-g++)

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

